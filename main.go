package main

import (
	"log"
	"github.com/gorilla/websocket"
	"net/http"
	"time"
	"fmt"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {
	log.Println("Starting...")
	http.HandleFunc("/echo", echo)
	http.HandleFunc("/flood", flood)

	log.Fatal(http.ListenAndServe("localhost:8080", nil))
}

func echo(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()
	for {
		mt, message, err := c.ReadMessage()

		if err != nil {
			log.Println("read:", err)
			break
		}

		log.Printf("recv: %s", message)

		err = c.WriteMessage(mt, message)
		if err != nil {
			log.Println("write:", err)
			break
		}

	}
}

func flood(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()

	i := 1
	for {
		err = c.WriteMessage(1, []byte(fmt.Sprintf("Test%d", i)))
		if err != nil {
			log.Println("write:", err)
			break
		}
		i++
		time.Sleep(50000 * time.Nanosecond)
	}
}
